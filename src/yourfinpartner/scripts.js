// --------------------------------------------------------------------------
// Scripts for Header
// --------------------------------------------------------------------------

$(function() {

    
    // --------------------------------------------------------------------------
	// Header Navigation
	// --------------------------------------------------------------------------

	var isTouch = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	$(document).on('mouseenter', '[data-nav]', function(event) {
		if ( !isTouch ) {
			var nav_id = $(this).data('nav');
			$('[data-nav=' + nav_id + ']').addClass('is-open');
		}
	})
	
	$(document).on('mouseleave', '[data-nav]', function(event) {
		if ( !isTouch ) {
			$('[data-nav]').removeClass('is-open');
		}
	});

	$(document).on('click', 'a[data-nav]', function(event) {
		event.preventDefault();

		if ( isTouch ) {
			var nav_id = $(this).data('nav');

			if ( $(this).is('.is-open') ) {
				$('[data-nav]').removeClass('is-open');
			} else {
				$('[data-nav=' + nav_id + ']').addClass('is-open');
			}
			
		}
		
	});

	
	$(document).on('click', function (event) {
		if ( $(event.target).closest('[data-nav]').length === 0) {
			$('[data-nav]').removeClass('is-open');
		}
    });
    
    
});