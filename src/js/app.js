$(function() {

	// --------------------------------------------------------------------------
	// Svg Polyfill
	// --------------------------------------------------------------------------

	svg4everybody();



	// --------------------------------------------------------------------------
	// Header Navigation
	// --------------------------------------------------------------------------

	var isTouch = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	$(document).on('mouseenter', '[data-nav]', function(event) {
		if ( !isTouch ) {
			var nav_id = $(this).data('nav');
			$('[data-nav=' + nav_id + ']').addClass('is-open');
		}
	})

	$(document).on('mouseleave', '[data-nav]', function(event) {
		if ( !isTouch ) {
			$('[data-nav]').removeClass('is-open');
		}
	});

	$(document).on('click', 'a[data-nav]', function(event) {
		event.preventDefault();

		if ( isTouch ) {
			var nav_id = $(this).data('nav');

			if ( $(this).is('.is-open') ) {
				$('[data-nav]').removeClass('is-open');
			} else {
				$('[data-nav=' + nav_id + ']').addClass('is-open');
			}

		}

	});


	$(document).on('click', function (event) {
		if ( $(event.target).closest('[data-nav]').length === 0) {
			$('[data-nav]').removeClass('is-open');
		}
	});


	// --------------------------------------------------------------------------
	// Swiper
	// --------------------------------------------------------------------------


	// Intro


	var swiperIntroOptions = {
		slidesPerView: 1,
		spaceBetween: 56,
		watchSlidesVisibility: true,
		watchOverflow: true,
		autoHeight: true,
		loop: true,
		navigation: {
			nextEl: '.js-swiper-intro-next',
			prevEl: '.js-swiper-intro-prev',
		},
		pagination: {
			el: '.js-swiper-intro-pagination',
			type: 'bullets',
		}
	}

	var swiperIntro = new Swiper('.js-swiper-intro', swiperIntroOptions);


	// programms


	if ( $('.js-swiper-programs').length ) {

		var swiperProgramsOptions = {
			slidesPerView: 1,
			spaceBetween: 30,
			watchSlidesVisibility: true,
			watchOverflow: true,
			navigation: {
				nextEl: '.js-swiper-programs-next',
				prevEl: '.js-swiper-programs-prev',
			},
			pagination: {
				el: '.js-swiper-programs-pagination',
				type: 'bullets',
			},
			breakpoints: {
				576: {
					slidesPerView: 2,
					spaceBetween: 30,
				},
				1280: {
					slidesPerView: 3,
					spaceBetween: 30,
				},

			}
		}

		var swiperPrograms = new Swiper('.js-swiper-programs', swiperProgramsOptions);

		if (window.matchMedia('(max-width: 575px)').matches ) {
			swiperPrograms.destroy(true, true);

		} else {
			swiperPrograms.destroy(true, true);
			swiperPrograms = new Swiper('.js-swiper-programs', swiperProgramsOptions);
		}

		$(window).on('resize orientationchange', function(event) {
			if (window.matchMedia('(max-width: 575px)').matches ) {
				swiperPrograms.destroy(true, true);
			} else {
				swiperPrograms.destroy(true, true);
				swiperPrograms = new Swiper('.js-swiper-programs', swiperProgramsOptions);
			}
		});

	}


	// Calc


	var swiperCalcOptions = {
		slidesPerView: 'auto',
		spaceBetween: 0,
		watchSlidesVisibility: true,
		watchOverflow: true,
		navigation: {
			nextEl: '.js-swiper-calc-next',
			prevEl: '.js-swiper-calc-prev',
		},
		pagination: {
			el: '.js-swiper-calc-pagination',
			type: 'bullets',
		}
	}

	var swiperCalc = new Swiper('.js-swiper-calc', swiperCalcOptions);


	// Articles

	var swiperArticlesOptions = {
		slidesPerView: 1,
		spaceBetween: 20,
		watchSlidesVisibility: true,
		watchOverflow: true,
		navigation: {
			nextEl: '.js-swiper-articles-next',
			prevEl: '.js-swiper-articles-prev',
		},
		pagination: {
			el: '.js-swiper-articles-pagination',
			type: 'bullets',
		},
		breakpoints: {
			768: {
				slidesPerView: 2,
				spaceBetween: 20,
			},
			992: {
				slidesPerView: 3,
				spaceBetween: 40,
			},
			1280: {
				slidesPerView: 4,
				spaceBetween: 40,
			},

		}
	}

	var swiperArticles = new Swiper('.js-swiper-articles', swiperArticlesOptions);

	// Reviews


	var swiperReviewsOptions = {
		slidesPerView: 1,
		spaceBetween: 20,
		watchSlidesVisibility: true,
		watchOverflow: true,
		autoHeight: false,
		navigation: {
			nextEl: '.js-swiper-reviews-next',
			prevEl: '.js-swiper-reviews-prev',
		},
		pagination: {
			el: '.js-swiper-reviews-pagination',
			type: 'bullets',
		},
		breakpoints: {
			768: {
				slidesPerView: 2,
				spaceBetween: 20,
			},
			992: {
				slidesPerView: 3,
				spaceBetween: 40,
			},
			1280: {
				slidesPerView: 4,
				spaceBetween: 40,
			},

		}
	}

	var swiperReviews = new Swiper('.js-swiper-reviews', swiperReviewsOptions);

	// benefits
	if ( $('.js-swiper-benefits').length ) {

		var swiperBenefitsOptions = {
			slidesPerView: 1,
			spaceBetween: 46,
			watchSlidesVisibility: true,
			watchOverflow: true,
			pagination: {
				el: '.js-swiper-benefits-pagination',
				type: 'bullets',
			},
			breakpoints: {
				992: {
					slidesPerView: 3,
				},

			}
		}

		var swiperBenefits = new Swiper('.js-swiper-benefits', swiperBenefitsOptions);

		if (window.matchMedia('(min-width: 768px)').matches ) {
			swiperBenefits.destroy(true, true);

		} else {
			swiperBenefits.destroy(true, true);
			swiperBenefits = new Swiper('.js-swiper-benefits', swiperBenefitsOptions);
		}

		$(window).on('resize orientationchange', function(event) {
			if (window.matchMedia('(min-width: 768px)').matches ) {
				swiperBenefits.destroy(true, true);
			} else {
				swiperBenefits.destroy(true, true);
				swiperBenefits = new Swiper('.js-swiper-benefits', swiperBenefitsOptions);
			}
		});

	}

	// Aspects

	if ( $('.js-swiper-aspects').length ) {

		var swiperAspectsOptions = {
			slidesPerView: 1,
			spaceBetween: 46,
			watchSlidesVisibility: true,
			watchOverflow: true,
			autoHeight: true,
			pagination: {
				el: '.js-swiper-aspects-pagination',
				type: 'bullets',
			}
		}

		var swiperAspects = new Swiper('.js-swiper-aspects', swiperAspectsOptions);

		if (window.matchMedia('(min-width: 992px)').matches ) {
			swiperAspects.destroy(true, true);

		} else {
			swiperAspects.destroy(true, true);
			swiperAspects = new Swiper('.js-swiper-aspects', swiperAspectsOptions);
		}

		$(window).on('resize orientationchange', function(event) {
			if (window.matchMedia('(min-width: 992px)').matches ) {
				swiperAspects.destroy(true, true);
			} else {
				swiperAspects.destroy(true, true);
				swiperAspects = new Swiper('.js-swiper-aspects', swiperAspectsOptions);
			}
		});

	}


	// Steps

	if ( $('.js-swiper-steps').length ) {

		var swiperStepsOptions = {
			slidesPerView: 1,
			spaceBetween: 56,
			watchSlidesVisibility: true,
			watchOverflow: true,
			autoHeight: true,
			pagination: {
				el: '.js-swiper-steps-pagination',
				type: 'bullets',
			},
			breakpoints: {
				768: {
					slidesPerView: 2,
				}
			}
		}

		var swiperSteps = new Swiper('.js-swiper-steps', swiperStepsOptions);

		if (window.matchMedia('(min-width: 1280px)').matches ) {
			swiperSteps.destroy(true, true);

		} else {
			swiperSteps.destroy(true, true);
			swiperSteps = new Swiper('.js-swiper-steps', swiperStepsOptions);
		}

		$(window).on('resize orientationchange', function(event) {
			if (window.matchMedia('(min-width: 1280px)').matches ) {
				swiperSteps.destroy(true, true);

			} else {
				swiperSteps.destroy(true, true);
				swiperSteps = new Swiper('.js-swiper-steps', swiperStepsOptions);

			}
		});

	}


	if ( $('.js-swiper-examples').length ) {

		var swiperExamplesOptions = {
			slidesPerView: 1,
			spaceBetween: 32,
			watchSlidesVisibility: true,
			watchOverflow: true,
			autoHeight: true,
			loop: true,
			navigation: {
				nextEl: '.js-swiper-examples-next',
				prevEl: '.js-swiper-examples-prev',
			},
			breakpoints: {
				576: {
					slidesPerView: 2,
					spaceBetween: 32,
				},
				1200: {
					slidesPerView: 3,
					spaceBetween: 20,
				}
			}
		}

		var swiperExamples = new Swiper('.js-swiper-examples', swiperExamplesOptions);


	}

	// --------------------------------------------------------------------------
	// Range
	// --------------------------------------------------------------------------

	$('input[type=range]').ionRangeSlider({
		skin: 'calc',
		type: 'single',
		prettify_enabled: true,
		hide_min_max: true,
		onChange: function(data){
			let loanSum = parseInt($('input[name=loan_sum]').val().replaceAll(' ', ''));
			let loanPeriod = parseInt($('input[name=loan_period]').val().replaceAll(' ', ''));
			if (data.slider[0].closest('.js-irs-0')) {
				loanSum = data.from;
			} else {
				loanPeriod = data.from;
			}

			let loanPercentVal = $('input[name=loan_percent]').val();
			let loanPercent = 0.025;
			if (typeof loanPercentVal !== 'undefined') {
				loanPercent = parseFloat(loanPercentVal);
			}
			let result;

			if (loanSum && loanPeriod) {
				result = (loanSum / loanPeriod + loanSum * loanPercent);
				result = Math.round(result);
				result = result.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ') + ' ₽';

				$('.calc__form-total span').html(result);
			}

			$(data.input).closest('.ui-range').find('.ui-input').val(data.from_pretty);
		}
	});

	$('.ui-range .ui-input').on('input', function(event) {

		var val = this.value.replace(/[^0-9]/g, '');

		$(this).closest('.ui-range ').find('input[type=range]').data('ionRangeSlider').update({
			from: val,
		});

		$(this).val( FormatPrice(val) );

	});

	// --------------------------------------------------------------------------
	// FormatPrice
	// --------------------------------------------------------------------------

	function FormatPrice(n) {
		return (n + "").split("").reverse().join("").replace(/(\d{3})/g, "$1 ").split("").reverse().join("").replace(/^ /, "");
	}

	// --------------------------------------------------------------------------
	// Placeholder
	// --------------------------------------------------------------------------


	$('.ui-placeholder__input, .ui-placeholder__textarea').each(function (index, value) {
		if ( $(this).val() != "" ) {
			$(this).closest('.ui-placeholder').addClass('is-changed');
		}
	});

	$('.ui-placeholder__input, .ui-placeholder__textarea').on('focusin', function(event) {
		$(this).closest('.ui-placeholder').addClass('is-focus');
	});

	$('.ui-placeholder__input, .ui-placeholder__textarea').on('focusout', function(event) {
		$(this).closest('.ui-placeholder').removeClass('is-focus');
		if( $(this).val() != "" ) {
			$(this).closest('.ui-placeholder').addClass('is-changed');
		} else{
			$(this).closest('.ui-placeholder').removeClass('is-changed');
		}
	});




	// --------------------------------------------------------------------------
	// Readmore
	// --------------------------------------------------------------------------

	$('.reviews__item-text').readmore({
		speed: 250,
		collapsedHeight: false,
		heightMargin: 16,
		moreLink: '<div class="reviews__item-readmore"><a href="#" class="reviews__item-readmore-link">Читать далее</a></div>',
		lessLink: '<div class="reviews__item-readmore"><a href="#" class="reviews__item-readmore-link">Скрыть</a></div>',
		embedCSS: true,
		startOpen: false,
		beforeToggle: function(trigger, element, expanded) {
			if(!expanded) {
				$(element).closest('.reviews__item').addClass('is-active');
			} else {
				$(element).closest('.reviews__item').removeClass('is-active');
			}
		},
		afterToggle: function(trigger, element, expanded) {

		},
		blockProcessed: function() {

		},
	});

	// --------------------------------------------------------------------------
	//  Isotope
	// --------------------------------------------------------------------------


	var grid = $('.reviews__grid').isotope({
		itemSelector: 'li',
		layoutMode: 'packery',
	});


	// $('.reviews__body').css('max-height', '648px');

	// --------------------------------------------------------------------------
	// Tabs
	// --------------------------------------------------------------------------

	$(document).on('click', '[data-tabs-btn]', function(event) {

		var tab = $(this),
			tab_id = $(this).attr('data-tabs-btn');

		$(this).closest('[data-tabs]').find('[data-tabs-btn], [data-tabs-content]').removeClass('is-active');
		$(this).closest('[data-tabs]').find('[data-tabs-btn=' + tab_id + '], [data-tabs-content=' + tab_id + ']').addClass('is-active');

	});


	// --------------------------------------------------------------------------
	// Callapse
	// --------------------------------------------------------------------------

	$(document).on('click', '.faq__item-toggle', function(event) {

		event.preventDefault();

		if ( $(this).closest('.faq__item').is('.is-open') ) {
			$(this).closest('.faq__item').removeClass('is-open').find('.faq__item-content').slideUp('fast');
		} else {
			$(this).closest('.faq__item').addClass('is-open').find('.faq__item-content').slideDown('fast');
		}

	});







	// --------------------------------------------------------------------------
	// Fancybox
	// --------------------------------------------------------------------------

	var fancyboxOptions = {
		infobar : false,
		toolbar : true,
		clickOutside: true,
		touch: false,
		animationEffect: false,
		animationDuration: 0,
		transitionEffect : false,
		transitionDuration: 0,
		lang: 'ru',
		smallBtn: false,
		closeExisting: true,
		hideScrollbar: true,
		preventCaptionOverlap: true,
		autoFocus: false,
		backFocus: false,
		loop: true,
		hash: false,
		arrows: false,
		wheel: false,
		buttons: [
			'close'
		],
		image: {
			preload: false
		},
		thumbs: {
			autoStart: true,
			hideOnClose: true,
			parentEl: ".fancybox-container",
			axis: "y"
		},
		i18n: {
			ru: {
				CLOSE: "Закрыть",
				NEXT: "Вперед",
				PREV: "Назад",
				ERROR: "Запрашиваемый контент не может быть загружен. <br/> Пожалуйста, попробуйте позже.",
			}
		},
		btnTpl: {
			/*close:
				'<button data-fancybox-close class="fancybox-btn fancybox-btn-close" title="{{CLOSE}}">' +
		         '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20 4L4 20" stroke="currentColor"/><path d="M20 20L4 4" stroke="currentColor"/></svg>' +
		        "</button>",*/

			arrowLeft:
				'<button data-fancybox-prev class="fancybox-btn fancybox-btn-prev" title="{{PREV}}">' +
				'<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16 4L8 12L16 20" stroke="currentColor"/></svg>' +
				"</button>",

			arrowRight:
				'<button data-fancybox-next class="fancybox-btn fancybox-btn-next" title="{{NEXT}}">' +
				'<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 20L16 12L8 4" stroke="currentColor"/></svg>' +
				"</button>",
		},
		beforeShow: function( instance, current ) {


			// var compensateCSS = $('.compensate-for-scrollbar').css('margin-right');

			// $('.header').css({
			//     'margin-right': compensateCSS
			// });

			$('body').removeClass('fancybox-type-inline fancybox-type-image');

			if ( current.type === "inline" ) {
				$('body').addClass('fancybox-type-inline');
			}

			if ( current.type === "image" ) {
				$('body').addClass('fancybox-type-image');

			}


		},
		afterClose: function( instance, current ) {


			$('body').removeClass('fancybox-type-inline fancybox-type-image');

			// $('.header').css({
			//     'margin-right': 0
			// });

		}
	}

	$('[data-fancybox]').fancybox(fancyboxOptions);


	// --------------------------------------------------------------------------
	// Validate
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Validate
	// --------------------------------------------------------------------------

	$('input[type="tel"]').inputmask({"mask": "+7 (999) 999-99-99", showMaskOnFocus: true, showMaskOnHover: false });

	$.validator.addMethod("regexpTel", function (value, element) {
		return this.optional(element) || /^\+\d \(\d{3}\) \d{3}-\d{2}-\d{2}$/.test(value);
	});

	$.validator.addMethod('regexpEmail', function(value, element) {
		return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
	});

	var validateErrorPlacement = function(error, element) {
		error.addClass('ui-validate');
		error.appendTo(element.parent());
	}

	var validateHighlight = function(element) {
		$(element)
			.parent().addClass("is-error").removeClass('is-success').find('.ui-validate').remove();
	}
	var validateUnhighlight = function(element) {
		$(element)
			.parent().addClass('is-success').removeClass("is-error").find('.ui-validate').remove();
	}


	$('.js-validate').each(function (index, value) {

		$(this).validate({
			errorElement: "span",
			ignore: ":disabled,:hidden",
			highlight: validateHighlight,
			unhighlight: validateUnhighlight,
			rules: {
				name: {
					required: true
				},
				phone: {
					required: true,
					regexpTel: true
				},
				email: {
					required: true,
					regexpEmail: true
				},
				comment: {
					required: true
				},
				agree: {
					required: true
				}
			},
			messages: {
				name: 'Введите имя',
				phone: 'Введите номер телефона',
				email: 'Введен некорректный адрес электронной почты',
				comment: 'Введите ваше сообщение',
				agree: 'Согласитесь с условиями',
			},
			errorPlacement: validateErrorPlacement,
			submitHandler: function(form) {
				const formData = {}
				const formArray = $(form).serializeArray();
				$(form).find('.ui-btn').attr('disabled', true);
				for (let item of formArray) {
					const fieldName = $(form).find('[name="' + item['name'] + '"]').data('result-name')
					formData[item['name']] = item['value'];
					if (typeof fieldName !== 'undefined') {
						formData[fieldName] = item['value'];
					}
				}

				$.ajax({
					url: '/ajax/landing_form.php',
					type: 'POST',
					data: formData,
					success: function (response) {
						$(form).find('.ui-btn').attr('disabled', false);
						$.fancybox.close();
						console.log(response);
						if ($(form).hasClass('review') ) {
							$.fancybox.open({src: '#popup-success-review'});
						} else {
							$.fancybox.open({src: '#popup-success'});
						}

					}
				});
				event.preventDefault();
			}
		});

	});
	$(document).ready(function () {
		let loanSum = parseInt($('input[name=loan_sum]').val().replaceAll(' ', ''));
		let loanPeriod = parseInt($('input[name=loan_period]').val().replaceAll(' ', ''));
		let loanPercentVal = $('input[name=loan_percent]').val();
		let loanPercent = 0.025;
		if (typeof loanPercentVal !== 'undefined') {
			loanPercent = parseFloat(loanPercentVal);
		}
		let result;
		if (loanSum && loanPeriod) {
			result = (loanSum / loanPeriod + loanSum * loanPercent);
			result = Math.round(result);
			result = result.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ') + ' ₽';

			$('.calc__form-total span').html(result);
		}
	})
	$('input[name=loan_sum]').trigger('change');
	window.cars = {};
	$.ajax({
		type: 'GET',
		url: '/cars.json',
		dataType: 'json',
		success(data) {
			window.cars = data;
		}
	});

	// --------------------------------------------------------------------------
	// Select
	// --------------------------------------------------------------------------

	$('select').selectric({
		maxHeight: 'auto',
		keySearchTimeout: 500,
		arrowButtonMarkup: '<span class="arrow"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6 10L12 15L18 10" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></span>',
		disableOnMobile: false,
		nativeOnMobile: false,
		openOnHover: false,
		hoverIntentTimeout: 500,
		expandToItemText: false,
		responsive: true,
		customClass: {
			prefix: 'selectric',
			camelCase: false
		},
		optionsItemBuilder: '{text}',
		labelBuilder: '{text}',
		preventWindowScroll: false,
		inheritOriginalWidth: false,
		allowWrap: true,
		multiple: {
			separator: ', ',
			keepMenuOpen: true,
			maxLabelEntries: false
		},
		optionsItemBuilder: function(itemData, element, index) {

			var select     = $(itemData.element).closest('select'),
				selectType = select.is('[multiple]');

			return '<span class="selectric-checked"><svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 7.5L6 12.5L17.5 1" stroke="currentColor"/></svg></span>' + itemData.text;

		},
		labelBuilder: function(itemData, index) {

			return itemData.text;

		},
		onBeforeInit: function(data) {

		},
		onInit: function(itemData, element, index) {

		},
		onBeforeOpen: function(element) {

		},
		onOpen: function(element) {

		},
		onBeforeClose: function() {

		},
		onClose: function(element) {

		},
		onBeforeChange: function() {

		},
		onChange: function(element, data) {
			if (element.name === 'brand' || element.name === 'mark') {
				const $modelSelect = $('select[name=model]');
				const selectedVendor = element.value;
				$.each(window.cars, (vendor, cars) => {
					if (vendor === selectedVendor) {
						$modelSelect.empty();
						$.each(cars, (i, carName) => {
							$modelSelect.append(`<option value="${carName}">${carName}</option>`);
						});
						$modelSelect.selectric('refresh');
					}
				});
			}
		},
		onRefresh: function() {

		},
	});
	// --------------------------------------------------------------------------
	// Loaded
	// --------------------------------------------------------------------------


	$('html').addClass('is-loaded');
});


$(document).ready(function(){


	$( ".close_link" ).on( "click", function() {
		BX.setCookie('consent_to_cookies', 'yes', {expires: 365*24*3600, path: '/', sameSite: 'Lax', secure: true});
		$('.wrap-text-cookie').css('display', 'none');
		$('.wrap-text-cookie-mobile').css('display', 'none');
	});

	$(window).scroll(function(){
		var footerHeight = 836;
		var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		var contentHeight = $(document).height() - window.innerHeight;
		if (scrollTop < contentHeight - footerHeight) {
			$('.wrap-text-cookie').css({bottom: 0, position: 'fixed'});
		} else {
			$('.wrap-text-cookie').css({bottom: 751, position: 'absolute'});
		}
	});
});